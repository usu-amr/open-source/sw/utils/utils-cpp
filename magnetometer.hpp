#pragma once

#include <chrono>

struct magData	//struct for containing magnetometer data
{
    
    std::chrono::high_resolution_clock::time_point timeStamp; //Timestamp for data
    double magX, magY, magZ;
};


class HMC5883L
{
private:
    magData current;
    double gainVal;
    int file;
public:
    enum class AveragedSamples
    {
        _1,
        _2,
        _4,
        _8
    };
    enum class DataRate 
    {
        _0_75_Hz,
        _1_5_Hz,
        _3_Hz,
        _7_5_Hz,
        _15_Hz,
        _30_Hz,
        _75_Hz
    };
    enum class MeasurementMode
    {
        NORMAL,
        POSITIVE_BIAS,
        NEGATIVE_BIAS
    };
    enum class Gain
    {
        _0_88_Ga,
        _1_3_Ga,
        _1_9_Ga,
        _2_5_Ga,
        _4_Ga,
        _4_7_Ga,
        _5_6_Ga,
        _8_1_Ga
    };
    enum class I2C_Rate
    {
        HIGH_SPEED,
        NORMAL_SPEED
    };
    enum class OperatingMode
    {
        CONTINOUS,
        SINGLE,
        IDLE
    };
    
    HMC5883L();
    
    HMC5883L(
        AveragedSamples averagedSamples, 
        DataRate dataRate, 
        MeasurementMode measurementMode, 
        Gain gain, 
        I2C_Rate I2C_rate, 
        OperatingMode operatingMode,
        int adapter_nr
    );
    
    bool dataAvailable();
    magData getMagData();
    
};
