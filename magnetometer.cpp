#include <iostream>
#include <cstdio>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <cstdlib>
#include "magnetometer.hpp"

HMC5883L::HMC5883L() : HMC5883L(
    AveragedSamples::_8, 
    DataRate::_15_Hz, 
    MeasurementMode::NORMAL, 
    Gain::_0_88_Ga, 
    I2C_Rate::NORMAL_SPEED, 
    OperatingMode::CONTINOUS,
    1
) {}		//When constructed with no options, use default values from the datasheet, 8 averages samples, 15Hz sample rate, no bias on measurement, range of +- 0.88 gauss (Not exact to datasheet, datasheet reccomends +-1.3), Normal I2C communication, Continous measurement mode, and the unix adapter is /dev/i2c-1
    
HMC5883L::HMC5883L(
    AveragedSamples averagedSamples, 
    DataRate dataRate, 
    MeasurementMode measurementMode, 
    Gain gain, 
    I2C_Rate I2C_rate, 
    OperatingMode operatingMode,
    int adapter_nr
)
{
    uint8_t configRegA, configRegB, modeReg;	//initalize variables to store register values
    configRegA = configRegB = modeReg = 0;
    
    int addr = 0x1E;	//As far as i can tell this is the only address that the magnetometer chip is on
    char filename[20];	//buffer for the filename in unix
    
    std::snprintf(filename, 19, "/dev/i2c-%d", adapter_nr);
    this->file = open(filename, O_RDWR);	//open file representing the I2C bus
    
    if(this->file < 0)
    {
        throw new std::runtime_error("Failed to open i2c adapter " + adapter_nr);	//error opening I2C adapter
    }
    if(ioctl(this->file, I2C_SLAVE, addr) < 0)
    {
        throw new std::runtime_error("Failed to communicate with I2C magnetometer at " + addr);	//error communicating with the I2C device
    }
    
    switch(averagedSamples)	//values between here 
    {
        case AveragedSamples::_1: break;
        case AveragedSamples::_2: configRegA += 0x20; break;
        case AveragedSamples::_4: configRegA += 0x40; break;
        case AveragedSamples::_8: configRegA += 0x60; break;
    }
    switch(dataRate)
    {
        case DataRate::_0_75_Hz: break;
        case DataRate::_1_5_Hz: configRegA += 0x04; break;
        case DataRate::_3_Hz: configRegA += 0x08; break;
        case DataRate::_7_5_Hz: configRegA += 0x0C; break;
        case DataRate::_15_Hz: configRegA += 0x10; break;
        case DataRate::_30_Hz: configRegA += 0x14; break;
        case DataRate::_75_Hz: configRegA += 0x18; break;
    }
    switch(measurementMode)
    {
        case MeasurementMode::NORMAL: break;
        case MeasurementMode::POSITIVE_BIAS: configRegA += 0x01; break;
        case MeasurementMode::NEGATIVE_BIAS: configRegA += 0x02; break;
    }
    switch(gain)
    {
        case Gain::_0_88_Ga: this->gainVal = 0.73; break;
        case Gain::_1_3_Ga: configRegB += 0x20; this->gainVal = 0.92; break;
        case Gain::_1_9_Ga: configRegB += 0x40; this->gainVal = 1.22; break;
        case Gain::_2_5_Ga: configRegB += 0x60; this->gainVal = 1.52; break;
        case Gain::_4_Ga: configRegB += 0x80; this->gainVal = 2.27; break;
        case Gain::_4_7_Ga: configRegB += 0xA0; this->gainVal = 2.56;break;
        case Gain::_5_6_Ga: configRegB += 0xC0; this->gainVal = 3.03; break;
        case Gain::_8_1_Ga: configRegB += 0xE0; this->gainVal = 4.35; break;
    }
    switch(I2C_rate)
    {
        case I2C_Rate::HIGH_SPEED: modeReg += 0x80; break;
        case I2C_Rate::NORMAL_SPEED: break;
    }
    switch(operatingMode)
    {
        case OperatingMode::CONTINOUS: break;
        case OperatingMode::SINGLE: modeReg += 0x01; break;
        case OperatingMode::IDLE: modeReg += 0x02; break;
    }								//and here are from the datasheet
    i2c_smbus_write_byte_data(this->file, 0x00, configRegA);	//write data to Magnetometer
    i2c_smbus_write_byte_data(this->file, 0x01, configRegB);
    i2c_smbus_write_byte_data(this->file, 0x02, modeReg);
}
    
bool HMC5883L::dataAvailable()
{
    return ((i2c_smbus_read_byte_data(this->file, 0x09)%2)!=0);	//check the data ready bit on the magnetometer
}


magData HMC5883L::getMagData()
{
    magData output;
    if(dataAvailable())
    {
         this->current.magX = ((i2c_smbus_read_byte_data(this->file, 0x03) << 8) | (i2c_smbus_read_byte_data(this->file, 0x04)))*this->gainVal;	//get data from magnetometer and multiply by a constant (from the datasheet) to get miliGauss
         this->current.magY = ((i2c_smbus_read_byte_data(this->file, 0x05) << 8) | (i2c_smbus_read_byte_data(this->file, 0x06)))*this->gainVal;
         this->current.magZ = ((i2c_smbus_read_byte_data(this->file, 0x07) << 8) | (i2c_smbus_read_byte_data(this->file, 0x08)))*this->gainVal;
         this->current.timeStamp = std::chrono::high_resolution_clock::now();
    }
    return this->current;	//return data as a magData struct
}
